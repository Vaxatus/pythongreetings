# Importuje moduł tkinter do wyświetlania GUI
from tkinter import *
# Importuje klasę tworzącą listę stringów przywitań od każdej osoby
from Persons import ListMaker
# Importuje przetworzony podułem pillow plik .gif zawierający animowane logo
from animeGIF import AnimatedGIF

# UWAGA! DO URUCHOMIENIA KODU POTRZEBNY MODUŁ PILLOW! ("pip install Pillow")

# Tworzy obiekt okna z modułu tkinter
window = Tk()

# Ustawia nazwę programu
window.title("Powitania")

# Ustawia rozmiar okna
window.geometry('280x450')

# Ustawia czcionkę
default_font = ('Segoe UI', 10)

# Tworzy przycisk, który po naciśnięciu dla każdego elementu listy wygenerowanej klasą ListMaker tworzy
# w swoje miejsce etykiety tkintera.
# Z każdą iteracją pętli "for" zmienia się numet rzędu (self.person_row), w którym generuje się tekst

class ShowingButton:
    def __init__(self):
        window.geometry('220x80')
        self.buttonShowingGreetings = Button(window, text="Pokaż powitania!", font=(default_font), command=self.showContent)
        self.buttonShowingGreetings.grid(column=0, row=0, padx=55, pady=25)

    def showContent(self):
        window.geometry('280x470')
        self.buttonShowingGreetings.destroy()
        self.listMaker = ListMaker()
        self.lista = self.listMaker.returnAlist()
        self.person_row = 0
        for persons in self.lista:
            self.label = Label(window, text=(persons.hello()), font=(default_font))
            self.label.grid(column=0, row=self.person_row, padx=10, pady=1)
            self.person_row += 1
        # Poniżej kod odpowiedzialny za pokazywanie logo z modułu Pillow (plik animeGIF.py)
        window.l_image = AnimatedGIF(window, "python2.gif")
        window.l_image.grid(row=self.person_row, column=0, sticky=N)
        window.l_image.pack()

if __name__ == "__main__":

    # Uruchamia nasze etykiety tkintera :)
    ShowingButton()

    # Nie wyrzucać! Główna pętla programu!
    window.mainloop()