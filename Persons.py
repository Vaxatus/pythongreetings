from JacekZ import Hello
from DominikW import Halo
from MarzenaSz import HelloM
from JakubG import Hello1
from MateuszK import Hi
from PiotrB import HelloPiotrB
from CezaryA import Hey
from OlgaC import Olga
from MagdaP import HelloMagda
from TomekG import HelloGrelo
from PatrykK import WitajJacek
from slawek import HelloSlawek
from TymekG import Hello45
from KrystianT import helloCris


# Żeby dodać kolejną linię z tekstem od następnej osoby, trzeba zaimportować u góry moduł,
# dodać ponizej według reszty linijkę "self.personKOLEJNY_NUMER = NazwaWaszejKlasy()", co stworzy
# kolejny obiekt person przypisany waszej klasie i dodać go do listy "list_of_persons"
# za pomocą "self.personToList(self.personKOLEJNY_NUMER, self.list_of_persons)".
#
# To wszystko :)


class ListMaker:
    def __init__(self):
        self.person1 = Hello()
        self.person2 = Halo()
        self.person3 = HelloM()
        self.person4 = Hello1()
        self.person5 = Hi()
        self.person6 = HelloPiotrB()
        self.person7 = Hey()
        self.person8 = Olga()
        self.person9 = HelloMagda()
        self.person10 = HelloGrelo()
        self.person11 = WitajJacek()
        self.person12 = HelloSlawek()
        self.person13 = Hello45()
        self.person14 = helloCris()
        self.list_of_persons = []
        self.personToList(self.person1, self.list_of_persons)
        self.personToList(self.person2, self.list_of_persons)
        self.personToList(self.person3, self.list_of_persons)
        self.personToList(self.person4, self.list_of_persons)
        self.personToList(self.person5, self.list_of_persons)
        self.personToList(self.person6, self.list_of_persons)
        self.personToList(self.person7, self.list_of_persons)
        self.personToList(self.person8, self.list_of_persons)
        self.personToList(self.person9, self.list_of_persons)
        self.personToList(self.person10, self.list_of_persons)
        self.personToList(self.person11, self.list_of_persons)
        self.personToList(self.person12, self.list_of_persons)
        self.personToList(self.person13, self.list_of_persons)
        self.personToList(self.person14, self.list_of_persons)

    def personToList(self, person, list):
        list.append(person)

    def returnAlist(self):
        return self.list_of_persons
